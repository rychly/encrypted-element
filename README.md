# The Encrypted (HTML) Element Project

*	Author: [Marek Rychly](mailto:marek.rychly@gmail.com)
*	License: [GPLv3](https://www.gnu.org/licenses/gpl.html) (see [LICENSE file](LICENSE))

The project implements an encryption and decryption of static HTML web-pages.
HTML content to be encrypted and decrypted can be enclosed into a special element.
Also `HREF` links in an `A` HTML element can be encrypted and decrypted without the necessity to encrypt the whole `A` element.
The encryption is performed by an external OpenSSL-based tool before the encrypted web-page is deployed.
The decryption is performed in a web-browser by HTML5 WebCrypt API functions on a user's request.

## HTML Content Encryption

The encryption is performed by running [encrypted-element.sh](encrypted-element.sh) tool, for example:

~~~
./encrypted-element.sh --file input.html
~~~

### DOM Sub-tree Encryption

An HTML content/source code in `x-encrypt` element will be encrypted by a password, IV, and salt provided in the element's attributes.
For example:

~~~html
<x-encrypt data-salt="0bbbbdcdf0137e47" data-iv="fc87b29f6cf2ac9b70aa92e43a150f12" data-password="test">test is <b>bold</b></x-encrypt>
~~~

### A HREF Encryption

An `HREF` link with the `x-encrypt:` prefix in an HTML `A` element can be encrypted by a password, IV, and salt provided in the element's attributes.
For example:

~~~html
<a href="x-encrypt:https://rychly.gitlab.io/" data-salt="0bbbbdcdf0137e47" data-iv="fc87b29f6cf2ac9b70aa92e43a150f12" data-password="test">encrypted link</a>
~~~

### Default Password, IV, and Salt

A default password, IV, and salt for the encryption can be set in `x-encrypt-param` element.
For example:

~~~html
<x-encrypt-param name="data-password">test</x-encrypt-param>
~~~

The default values can be reset by specifying `data-salt`, `data-iv`, and `data-password` attributes,
or by the encryption script command line parameters.

## HTML Content Decryption

The encryption is performed by HTML5 WebCrypt API function calls in
[encrypted-element.js](encrypted-element.js) JavaScript file included into the encrypted web-pages.

### Visualization Templates

Before the decryption, the encrypted HTML content is visualized as set in `template-for-encrypted` HTML5 template.
For example:

~~~html
<template id="template-for-encrypted">
<p>(encrypted)</p>
</template>
~~~

On access of the encrypted content by a user, the user is asked for the right password as set in `template-for-decryption` HTML5 template.
For example:

~~~html
<template id="template-for-decryption">
<form onsubmit="return false"><label>Password: <input type="password" name=""/></label><button type="submit">Decrypt</button></form>
</template>
~~~

### Decryption in a Web Browser

The encrypted HTML content (and links) is decrypted individually on demand, after a user access the encrypted content (see the templates above).
All the content, that is encrypted by the same password, can be decrypted in a one step
by calling `encryptedElement.decryptAllByPasswordOnSubmit(this);` from the on-submit action of a HTML form with a "password" input box.
For example:

~~~html
<form onsubmit="return encryptedElement.decryptAllByPasswordOnSubmit(this);">
<h2>Decrypt all</h2>
<label>Password: <input type="password"/></label>
<button type="submit">Decrypt all</button>
</form>
~~~
