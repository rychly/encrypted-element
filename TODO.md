## TODO

*	disable encrypted href click by http://stackoverflow.com/a/13977403
*	switch from Custom Elements (https://w3c.github.io/webcomponents/spec/custom/)
	*	v0 (http://www.html5rocks.com/en/tutorials/webcomponents/customelements/) to
	*	v1 (https://developers.google.com/web/fundamentals/primers/customelements/)
	and/or use Polyfils (https://github.com/webcomponents/webcomponentsjs/tree/v1)
*	remove createShadowRoot as it is deprecated (https://developer.mozilla.org/en-US/docs/Web/API/Element/createShadowRoot)
	and attachShadow is not yet implemented (https://developer.mozilla.org/en-US/docs/Web/API/Element/attachShadow)
*	use localStorage and sessionStorage to store a user's private key / keys for each descrypted page / content(dataURI) of decrypted pages, see
	*	https://www.w3.org/TR/webstorage/#storage-0
	*	http://www.w3schools.com/HTML/html5_webstorage.asp
*	enable off-line by appcache, see http://www.html5rocks.com/en/tutorials/appcache/beginner/
