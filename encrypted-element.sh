#!/bin/bash
#
# The Encrypted (HTML) Element Project
# Copyright (c) 2016, Marek Rychly <marek.rychly@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

# requires: dev-lang/perl, dev-libs/openssl, app-editors/vim-core (/usr/bin/xxd), pbkdf2.pl

ENCRYPT_ELEM_NAME="x-encrypt"
ENCRYPT_PARAM_ELEM_NAME="x-encrypt-param"
ENCRYPTED_ELEM_NAME="x-encrypted"
LINK_ELEM_NAME="a"
ENCRYPT_HREF_ATTR_PROTOCOL="x-encrypt"
ENCRYPTED_HREF_MEDIA="application/x-encrypted-uri"
ENCRYPTED_HREF_PREFIX="data:${ENCRYPTED_HREF_MEDIA};base64,"
PASSWORD_ATTR_NAME="data-password"
SALT_ATTR_NAME="data-salt"
IV_ATTR_NAME="data-iv"
PBKDF2_TOOL="$(dirname "${0}")/tools/pbkdf2.pl"

if [[ "${1}" == "--help" || $# -lt 1 || ! -x "${PBKDF2_TOOL}" ]]; then
	echo "Usage: ${0} [--href] [--salt <salt>] [--iv <iv>] <text> < password-file" >&2
	echo "Usage: ${0} [--href] [--salt <salt>] [--iv <iv>] --password <password> <text>" >&2
	echo "Convert a given text into encrypted <${ENCRYPTED_ELEM_NAME} ${SALT_ATTR_NAME}=\"...\" ${IV_ATTR_NAME}=\"...\">...</${ENCRYPTED_ELEM_NAME}> element, \
or, in the case of '--href' option, into element prefix <${LINK_ELEM_NAME} href=\"${ENCRYPTED_HREF_PREFIX}...\" ${SALT_ATTR_NAME}=\"...\" ${IV_ATTR_NAME}=\"...\", \
optionally with a given IV and salt hexadecimal values." >&2
	echo >&2
	echo "Usage: ${0} --file <input.html> [--password <password>]" >&2
	echo "In a given input file, convert all <${ENCRYPT_ELEM_NAME} ${SALT_ATTR_NAME}=\"...\" ${IV_ATTR_NAME}=\"...\" ${PASSWORD_ATTR_NAME}=\"...\">...</${ENCRYPT_ELEM_NAME}> \
into encrypted <${ENCRYPTED_ELEM_NAME} ${SALT_ATTR_NAME}=\"...\" ${IV_ATTR_NAME}=\"...\">...</${ENCRYPTED_ELEM_NAME}> elements, \
and all <${LINK_ELEM_NAME} href=\"${ENCRYPT_HREF_ATTR_PROTOCOL}:...\" ${SALT_ATTR_NAME}=\"...\" ${IV_ATTR_NAME}=\"...\" ${PASSWORD_ATTR_NAME}=\"...\" ... \
into encrypted <${LINK_ELEM_NAME} href=\"${ENCRYPTED_HREF_PREFIX}...\" ${SALT_ATTR_NAME}=\"...\" ${IV_ATTR_NAME}=\"...\" element prefixes." >&2
	echo "Attributes '${SALT_ATTR_NAME}' and '${IV_ATTR_NAME}' of elements '${ENCRYPT_ELEM_NAME}' and '${LINK_ELEM_NAME}' must be hexadecimal values and they are optional (random values by default)." >&2
	echo "In the case of '--password' parameter, the '${PASSWORD_ATTR_NAME}' attributes of the elements above are optional (the parameter's value will be used as the default password)." >&2
	echo "The default password can be also set in a one line of the given input file by <${ENCRYPT_PARAM_ELEM_NAME} name=\"${PASSWORD_ATTR_NAME}\">...</${ENCRYPT_PARAM_ELEM_NAME}> \
element (with a lower priority than the '--password' parameter above)." >&2
	echo >&2
	echo "The encryption will be performed by a one-line ASCII password by AES-CBC with a 256-bit key derived from the password by PBKDF2." >&2
	echo "A tool pbkdf2 is required to generate a cryptographic hash of the password, it should be located at ${PBKDF2_TOOL}." >&2
	exit 1
fi

# Usage: encrypt <mode> <salthex> <ivhex> <password> <data>
function encrypt() {
	local MODE="${1}"
	## get password and derive a PBKDF2 key of 32 bytes for AES-CBC 256-bit
	# webcrypt aletrnative:
	# 	var saltArrayBuffer = window.crypto.getRandomValues(new Uint8Array(8));
	# 	window.crypto.subtle.importKey("raw", stringToArrayBuffer(password), {name: "PBKDF2"}, false, ["deriveKey"])
	# 	.then(function(baseKey){return window.crypto.subtle.deriveKey({name: "PBKDF2", salt: saltArrayBuffer, iterations: 1000, hash: "SHA-1"}, baseKey, {name: "AES-CBC", length: 256}, true, ["encrypt", "decrypt"]);})
	# 	.then(function(aesKey){console.info(new TextDecoder("utf-8").decode(new DataView(aesKey)));});
	local SALT="${2}"
	local ITERATIONS=1000
	local KEY=$(echo "${4}" | "${PBKDF2_TOOL}" "${SALT}" "${ITERATIONS}")
	## generate a new IV
	# webcrypt aletrnative:
	# 	ivArrayBuffer = window.crypto.getRandomValues(new Uint8Array(16));
	local IV="${3}"
	shift 4
	## encrypt a file by AES-CBC 256-bit and the key and IV above
	# webcrypt aletrnative:
	# 	window.crypto.subtle.encrypt({name: "AES-CBC", iv: ivArrayBuffer}, aesKey, dataArrayBuffer).
	# 	.then(function(decrypted){console.info(new TextDecoder("utf-8").decode(new DataView(decrypted)), new TextDecoder("utf-8").decode(new DataView(ivArrayBuffer)));});
	case "${MODE}" in
		element)
			echo -n "<${ENCRYPTED_ELEM_NAME} ${SALT_ATTR_NAME}=\""
			echo -n "${SALT}" | xxd -r -p | base64 | tr -d '=\n'
			echo -n "\" ${IV_ATTR_NAME}=\""
			echo -n "${IV}" | xxd -r -p | base64 | tr -d '=\n'
			echo -n "\">"
			echo -n "${@}" | openssl enc -e -aes-256-cbc -K "${KEY}" -iv "${IV}" | base64 | tr -d '=\n'
			echo -n "</${ENCRYPTED_ELEM_NAME}>"
			;;
		href)
			#
			echo -n "<${LINK_ELEM_NAME} href=\"${ENCRYPTED_HREF_PREFIX}"
			echo -n "${@}" | openssl enc -e -aes-256-cbc -K "${KEY}" -iv "${IV}" | base64 | tr -d '=\n'
			echo -n "\" ${SALT_ATTR_NAME}=\""
			echo -n "${SALT}" | xxd -r -p | base64 | tr -d '=\n'
			echo -n "\" ${IV_ATTR_NAME}=\""
			echo -n "${IV}" | xxd -r -p | base64 | tr -d '=\n'
			echo -n "\""
			;;
		*)
			echo "Unknow output mode '${MODE}'" >&2
			exit 2
	esac
}

if [[ "${1}" == "--file" ]]; then
	FILE="${2}"
	shift 2
	PARAM_PASSWORD=$(grep -zo "<${ENCRYPT_PARAM_ELEM_NAME}\s\+name=\"${PASSWORD_ATTR_NAME}\">[^<>]*</${ENCRYPT_PARAM_ELEM_NAME}>" "${FILE}")
	if [[ -n "${PARAM_PASSWORD}" ]]; then
		PASSWORD_WITHOUT_OPENNING_TAG="${PARAM_PASSWORD#*>}" PASSWORD="${PASSWORD_WITHOUT_OPENNING_TAG%<*}"
	fi
	if [[ "${1}" == "--password" ]]; then
		PASSWORD="${2}"
		shift 2
	fi
	read -d '' PERL_SCRIPT <<EOF
s|<${ENCRYPT_ELEM_NAME}\\\\s+${SALT_ATTR_NAME}="([^"]*)"\\\\s+${IV_ATTR_NAME}="([^"]*)"\\\\s+${PASSWORD_ATTR_NAME}="([^"]*)">(.*?)</${ENCRYPT_ELEM_NAME}>|\`${0} --salt '\$1' --iv '\$2' --password '\$3' '\$4'\`|egs;
s|<${ENCRYPT_ELEM_NAME}\\\\s+${SALT_ATTR_NAME}="([^"]*)"\\\\s+${PASSWORD_ATTR_NAME}="([^"]*)">(.*?)</${ENCRYPT_ELEM_NAME}>|\`${0} --salt '\$1' --password '\$2' '\$3'\`|egs;
s|<${ENCRYPT_ELEM_NAME}\\\\s+${IV_ATTR_NAME}="([^"]*)"\\\\s+${PASSWORD_ATTR_NAME}="([^"]*)">(.*?)</${ENCRYPT_ELEM_NAME}>|\`${0} --iv '\$1' --password '\$2' '\$3'\`|egs;
s|<${ENCRYPT_ELEM_NAME}\\\\s+${PASSWORD_ATTR_NAME}="([^"]*)">(.*?)</${ENCRYPT_ELEM_NAME}>|\`${0} --password '\$1' '\$2'\`|egs;
s|<${LINK_ELEM_NAME}\\\\s+href="${ENCRYPT_HREF_ATTR_PROTOCOL}:([^"]*)"\\\\s+${SALT_ATTR_NAME}="([^"]*)"\\\\s+${IV_ATTR_NAME}="([^"]*)"\\\\s+${PASSWORD_ATTR_NAME}="([^"]*)"|\`${0} --href --salt '\$2' --iv '\$3' --password '\$4' '\$1'\`|egs;
s|<${LINK_ELEM_NAME}\\\\s+href="${ENCRYPT_HREF_ATTR_PROTOCOL}:([^"]*)"\\\\s+${SALT_ATTR_NAME}="([^"]*)"\\\\s+${PASSWORD_ATTR_NAME}="([^"]*)"|\`${0} --href --salt '\$2' --password '\$3' '\$1'\`|egs;
s|<${LINK_ELEM_NAME}\\\\s+href="${ENCRYPT_HREF_ATTR_PROTOCOL}:([^"]*)"\\\\s+${IV_ATTR_NAME}="([^"]*)"\\\\s+${PASSWORD_ATTR_NAME}="([^"]*)"|\`${0} --href --iv '\$2' --password '\$3' '\$1'\`|egs;
s|<${LINK_ELEM_NAME}\\\\s+href="${ENCRYPT_HREF_ATTR_PROTOCOL}:([^"]*)"\\\\s+${PASSWORD_ATTR_NAME}="([^"]*)"|\`${0} --href --password '\$2' '\$1'\`|egs;
if (length("${PASSWORD}")) {
	s|<${ENCRYPT_ELEM_NAME}\\\\s+${SALT_ATTR_NAME}="([^"]*)"\\\\s+${IV_ATTR_NAME}="([^"]*)">(.*?)</${ENCRYPT_ELEM_NAME}>|\`${0} --salt '\$1' --iv '\$2' --password '${PASSWORD}' '\$3'\`|egs;
	s|<${ENCRYPT_ELEM_NAME}\\\\s+${SALT_ATTR_NAME}="([^"]*)">(.*?)</${ENCRYPT_ELEM_NAME}>|\`${0} --salt '\$1' --password '${PASSWORD}' '\$2'\`|egs;
	s|<${ENCRYPT_ELEM_NAME}\\\\s+${IV_ATTR_NAME}="([^"]*)">(.*?)</${ENCRYPT_ELEM_NAME}>|\`${0} --iv '\$1' --password '${PASSWORD}' '\$2'\`|egs;
	s|<${ENCRYPT_ELEM_NAME}>(.*?)</${ENCRYPT_ELEM_NAME}>|\`${0} --password '\$1' '\$2'\`|egs;
	s|<${LINK_ELEM_NAME}\\\\s+href="${ENCRYPT_HREF_ATTR_PROTOCOL}:([^"]*)"\\\\s+${SALT_ATTR_NAME}="([^"]*)"\\\\s+${IV_ATTR_NAME}="([^"]*)"|\`${0} --href --salt '\$2' --iv '\$3' --password '${PASSWORD}' '\$1'\`|egs;
	s|<${LINK_ELEM_NAME}\\\\s+href="${ENCRYPT_HREF_ATTR_PROTOCOL}:([^"]*)"\\\\s+${SALT_ATTR_NAME}="([^"]*)"|\`${0} --href --salt '\$2' --password '${PASSWORD}' '\$1'\`|egs;
	s|<${LINK_ELEM_NAME}\\\\s+href="${ENCRYPT_HREF_ATTR_PROTOCOL}:([^"]*)"\\\\s+${IV_ATTR_NAME}="([^"]*)"|\`${0} --href --iv '\$2' --password '${PASSWORD}' '\$1'\`|egs;
	s|<${LINK_ELEM_NAME}\\\\s+href="${ENCRYPT_HREF_ATTR_PROTOCOL}:([^"]*)"|\`${0} --href --password '${PASSWORD}' '\$1'\`|egs;
}
EOF
	OUTPUT=$(grep -v "<${ENCRYPT_PARAM_ELEM_NAME}\s[^>]*>[^<>]*</${ENCRYPT_PARAM_ELEM_NAME}>" "${FILE}" | perl -00pe "${PERL_SCRIPT}")
	if `echo "${OUTPUT}" | grep -zq "<${ENCRYPT_PARAM_ELEM_NAME}\(\s\|\$\)\|<${ENCRYPT_ELEM_NAME}\(\s\|\$\)\|<${LINK_ELEM_NAME}\s[^>]*href=\"${ENCRYPT_HREF_ATTR_PROTOCOL}:"`; then
		echo "Not all encryption parameters or the content to ecrypt has been encrypted (it may be a syntax error or missing parameters)!" >&2
		exit 3
	else
		echo "${OUTPUT}"
	fi
else
	# href
	if [[ "${1}" == "--href" ]]; then
		MODE="href"
		shift
	else
		MODE="element"
	fi
	# salt
	if [[ "${1}" == "--salt" ]]; then
		SALT="${2}"
		shift 2
	else
		SALTBYTES=8 SALT=$(openssl rand "${SALTBYTES}" -hex)
	fi
	# IV
	if [[ "${1}" == "--iv" ]]; then
		IV="${2}"
		shift 2
	else
		IVBYTES=16 IV=$(openssl rand "${IVBYTES}" -hex)
	fi
	# password
	if [[ "${1}" == "--password" ]]; then
		PASSWORD="${2}"
		shift 2
	else
		read PASSWORD
	fi
	# encryption
	encrypt "${MODE}" "${SALT}" "${IV}" "${PASSWORD}" "${@}"
fi
