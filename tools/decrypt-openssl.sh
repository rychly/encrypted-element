#!/bin/bash
#
# The Encrypted (HTML) Element Project
# Copyright (c) 2016, Marek Rychly <marek.rychly@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

FILE_IN=${1}
FILE_OUT=${2}
DIR=$(dirname "${0}")

if [[ "${1}" == "--help" || ! -e "${FILE_IN}" ]]; then
	echo "Usage: ${0} input-file output-file < password-file" >&2
	echo "Decrypt a given input file to a given output file by a ASCII password from stdin by AES-CBC with a 256-bit key derived from the password by PBKDF2." >&2
	echo >&2
	echo "Usage: ${0} input-file output-file [hexadecimal-aes-256-bit-key]" >&2
	echo "Decrypt a given input file to a given output file by a given hexadecimal 256-bit key by AES-CBC (32 characters)." >&2
	exit 1
fi

if [[ $# != 3 ]]; then

	## read a password from stdin and derive a PBKDF2 key of 32 bytes for AES-CBC 256-bit
	# webcrypt aletrnative:
	# 	var saltArrayBuffer = new ArrayBuffer(8);
	# 	new Uint8Array(saltArrayBuffer).set([0x12,0x34,0x56,0x78,0x90,0xAB,0xCD,0xEF]);
	# 	window.crypto.subtle.importKey("raw", stringToArrayBuffer(password), {name: "PBKDF2"}, false, ["deriveKey"])
	# 	.then(function(baseKey){return window.crypto.subtle.deriveKey({name: "PBKDF2", salt: saltArrayBuffer, iterations: 1000, hash: "SHA-1"}, baseKey, {name: "AES-CBC", length: 256}, true, ["encrypt", "decrypt"]);})
	# 	.then(function(aesKey){console.info(new TextDecoder("utf-8").decode(new DataView(aesKey)));});
	SALT=1234567890ABCDEF
	ITERATIONS=1000
	KEY=$(${DIR}/pbkdf2.pl "${SALT}" "${ITERATIONS}")

else

	## get a hexadecimal 256-bit key by AES-CBC
	KEY="${3}"

fi

## read IV from the end of a file
# webcrypt aletrnative:
# 	ivArrayBuffer = new Uint8Array(dataBufferArray.slice(-16));
IVBYTES=16
IV=$(xxd -c 256 -g 256 -s "-${IVBYTES}" "${FILE_IN}" | cut -d ' ' -f 2)

## decrypt the rest of the file by AES-CBC 256-bit and the key and IV above
# webcrypt aletrnative:
# 	window.crypto.subtle.decrypt({name: "AES-CBC", iv: ivArrayBuffer}, aesKey, dataArrayBuffer.slice(0, -16))
# 	.then(function(decrypted){console.info(new TextDecoder("utf-8").decode(new DataView(decrypted)));});
echo "key=${KEY}"
echo "iv =${IV}"
exec openssl enc -d -aes-256-cbc -in <(head "--bytes=-${IVBYTES}" "${FILE_IN}") -out "${FILE_OUT}" -K "${KEY}" -iv "${IV}"
