## Design

This document describes a design proposal for the key management in HTML element encryption.
The following source-code fragments are in a JavaScript-like pseudocode.

~~~javascript

// after creating the user, his/her pages have to be reseted, otherwise he/she cannot access them
createUser(userName, userPassword) {
	(privateKey, publicKey) = generateRSAKeyPair();
	userPasswordKey = deriveAESKeyFromPassword(userPassword);
	privateKeyEncrypted = encryptAES(privateKey, userPasswordKey);
	saveToFile(privateKeyEncrypted, "/user/" + userName + ".privateKey.enc");
	saveToFile(publicKey, "/user/" + userName + ".publicKey");
}

createPage(pageName, content, pagePassword, users) {
	pagePasswordKey = pagePassword.isEmpty()
		? generateAESKey()
		: deriveAESKeyFromPassword(pagePassword);
	contentEncrypted = encryptAES(content, pagePasswordKey);
	saveToFile(contentEncrypted, "/content/" + pageName + ".enc");
	removeFile("/content/" + pageName + ".key4*.enc");
	foreach (userName in users) {
		publicKey = loadFromFile("/user/" + userName + ".publicKey");
		pagePasswordKeyEncrypted = encryptRSA(pagePasswordKey, publicKey);
		saveToFile(pagePasswordKeyEncrypted, "/content/" + pageName + ".key4" + userName + ".enc");
	}
}

readUserPrivateKey(userName, userPassword) {
	userPasswordKey = deriveAESKeyFromPassword(userPassword);
	privateKeyEncrypted = loadFromFile("/user/" + userName + ".privateKey.enc");
	privateKey = decryptAES(privateKeyEncrypted, userPasswordKey);
	return privateKey;
}

readPage(pageName, pagePassword, userName, userPassword) {
	if (!pagePassword.isEmpty()) {
		pagePasswordKey = deriveAESKeyFromPassword(pagePassword);
	} else if (!userPassword.isEmpty()) {
		privateKey = readUserPrivateKey(userName, userPassword);
		pagePasswordKeyEncrypted = loadFromFile("/content/" + pageName + ".key4" + userName + ".enc");
		pagePasswordKey = decryptRSA(pagePasswordKeyEncrypted, privateKey);
	} else {
		pagePasswordKey = loadFromBrowserKeyStore(pageName);
		if (pagePasswordKey.isEmpty()) {
			printf(stderr, "Enter the password!");
			return;
		}
	}
	contentEncrypted = loadFromFile("/content/" + pageName + ".enc");
	content = decryptAES(pagePasswordKey);
	if (content.isEmpty()) {
		printf(stderr, "Re-enter the password!");
		return;
	}
	saveToBrowserKeyStore(pagePasswordKey, pageName);
	return content;
}

// access can be reset only by those users who can read the page
resetPage(pageName, pagePassword, userName, userPassword, users) {
	content = readPage(pageName, pagePassword, userName, userPassword);
	if (!content.isEmpty()) {
		createPage(pageName, content, pagePassword, users);
	}
}

// need not to modify the list of users, just reset the page
resetPage(pageName, pagePassword, userName, userPassword) {
	users = [];
	foreach (file in listFiles("/content/" + pageName + ".key4*.enc")) {
		userName = extractUserNameFromPath(file);
		users += [ userName ];
	}
	resetPage(pageName, pagePassword, userName, userPassword, users);
}

removePage(pageName) {
	removeFile("/content/" + pageName + ".*");
}

// when reset user's known password to a given new password, it is not necessary to reset his/her pages (the privateKey is the same)
resetUser(userName, userPasswordOld, userPasswordNew) {
	privateKey = readUserPrivateKey(userName, userPasswordOld);
	userPasswordNewKey = deriveAESKeyFromPassword(userPasswordNew);
	privateKeyEncrypted = encryptAES(privateKey, userPasswordNewKey);
	saveToFile(privateKeyEncrypted, "/user/" + userName + ".privateKey.enc");
}

removeUser(userName) {
	removeFile("/content/*.key4" + userName + ".enc");
	removeFile("/user/" + userName + ".*");
}

// when reset user's lost password to a given new password, it is necessary to reset his/her pages (the private key is new)
resetUser(userName, userPasswordNew, userNameAdmin, userPasswordAdmin) {
	// do not remove the user as it will remove also keys for pages and access information would be lost
	// crateUser will overwrite the user's key files so the user removel is not needed
	//removeUser(userName);
	createUser(userName, userPasswordNew);
	foreach (file in listFiles("/content/*.key4" + userName + ".enc")) {
		pageName = extractPageNameFromPath(file);
		resetPage(pageName, null, userNameAdmin, userPasswordAdmin);
	}
}

~~~
