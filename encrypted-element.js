/*
 * The Encrypted (HTML) Element Project
 * Copyright (c) 2016, Marek Rychly <marek.rychly@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

var encryptedElement = new function() {

	"use strict";

	// HTML template selectors, custom element and attribute names, and MIME/media types
	var _templateForEncryptedSelector = '#template-for-encrypted';
	var _templateForDecryptionSelector = '#template-for-decryption';
	var _encryptedElementName = 'x-encrypted';
	var _saltAttributeName = 'data-salt';
	var _ivAttributeName = 'data-iv';
	var _encryptedHrefMedia = 'application/x-encrypted-uri';
	var _encryptedAElementSelector = 'a[href^="data:' + _encryptedHrefMedia + ';base64,"]';

	// hide the custom element iff unresolved
	{
		var _unresolvedStyleCss = _encryptedElementName + ':unresolved { opacity: 0; }';
		var _unresolvedStyleElement = document.createElement('style');
		if (_unresolvedStyleElement.styleSheet) _unresolvedStyleElement.styleSheet.cssText = _unresolvedStyleCss;
		else _unresolvedStyleElement.appendChild(document.createTextNode(_unresolvedStyleCss));
		document.getElementsByTagName('head')[0].appendChild(_unresolvedStyleElement);
	}

	// check for Crypto API, see https://developer.mozilla.org/en-US/docs/Web/API/Crypto
	if (!window.crypto || !window.crypto.subtle) {
		alert("Your browser does not support the Crypto API! This page will not work.");
		return;
	}
	// check for TextEncoder/TextDecoder API, see https://developer.mozilla.org/en-US/docs/Web/API/TextEncoder and https://developer.mozilla.org/en-US/docs/Web/API/TextDecoder
	if (!window.TextEncoder || !window.TextDecoder) {
		alert("Your browser does not support the TextEncoder/TextDecoder API! This page will not work.");
		return;
	}
	// check for Promise API, see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise
	if (!window.Promise) {
		alert("Your browser does not support the Promise API! This page will not work.");
		return;
	}
	// check for <template> element, see https://developer.mozilla.org/en-US/docs/Web/HTML/Element/template
	if (!('content' in document.createElement('template'))) {
		alert("Your browser does not support the <template> element! This page will not work.");
		return;
	}
	// check for ShadowRoot API, see https://developer.mozilla.org/en-US/docs/Web/API/ShadowRoot
	if (!window.ShadowRoot) {
		alert("Your browser does not support the ShadowRoot API! This page will not work.");
		return;
	}

	// default encryption parameters (IV must be 16 bytes)
	var _defaultIvArrayBuffer = new ArrayBuffer(16);
	var _defaultPbkdf2KeySaltArrayBuffer = new ArrayBuffer(8);
	new Uint8Array(_defaultIvArrayBuffer).set([0x12,0x34,0x56,0x78,0x90,0xAB,0xCD,0xEF,0x12,0x34,0x56,0x78,0x90,0xAB,0xCD,0xEF]);
	new Uint8Array(_defaultPbkdf2KeySaltArrayBuffer).set([0x12,0x34,0x56,0x78,0x90,0xAB,0xCD,0xEF]);

	// encrypted element prototype
	this.elementPrototype = Object.create(HTMLElement.prototype, {
		decryptByPassword: {
			value: function(password) {
				var passwordArrayBuffer = new TextEncoder("utf-8").encode(password);
				return this.decryptByPasswordArrayBuffer(passwordArrayBuffer);
			}
		},
		decryptByPasswordArrayBuffer: {
			value: function(passwordArrayBuffer) {
				// promises (async functions) cannot reference the local "this" variable
				var _this = this;
				// convert the element's content and attributes' base64-encoded values to ArrayBuffer values
				return Promise.all([
					// data
					encryptedElement.base64ToArrayBuffer(this.textContent),
					// salt
					this.hasAttribute(_saltAttributeName)
					? encryptedElement.base64ToArrayBuffer(this.getAttribute(_saltAttributeName))
					: _defaultSaltArrayBuffer,
					// iv
					this.hasAttribute(_ivAttributeName)
					? encryptedElement.base64ToArrayBuffer(this.getAttribute(_ivAttributeName))
					: _defaultIvArrayBuffer
				]).then(function(arrayBufferValues) {
					// decrypt
					return encryptedElement.decryptor(arrayBufferValues[0], arrayBufferValues[1],
						arrayBufferValues[2], passwordArrayBuffer);
				}).then(function(html) {
					// replace this element by the HTML plain-text (must be HTML otherwise there will be DOMException)
					// HTML5 template element is utilized as it can contain all types of HTML nodes
					var templateElement = document.createElement('template');
					templateElement.innerHTML = html;
					_this.parentNode.replaceChild(templateElement.content, _this);
				});
			}
		},
		createdCallback: {
			value: function() {
				// add DOM shadow to visualize the encrypted element
				var shadow = this.createShadowRoot();
				var templateForEncrypted = document.querySelector(_templateForEncryptedSelector);
				var nodeForEncrypted = templateForEncrypted.content.cloneNode(true);
				shadow.appendChild(nodeForEncrypted);
				// event handler functions cannot reference the local "this" variable
				var _this = this;
				this.addEventListener('click', function onClick(e) {
					// add the decryption form into the DOM shadow
					var templateForDecryption = document.querySelector(_templateForDecryptionSelector);
					var nodeForDecryption = templateForDecryption.content.cloneNode(true);
					nodeForDecryption.querySelector('form').onsubmit = function() {
						_this.decryptByPassword(this.elements[0].value).catch(alert);
						// do not submit the form
						return false;
					};
					shadow.appendChild(nodeForDecryption);
					// disable the onClick event handler as the form should not be added multiple-times
					if (this.removeEventListener) this.removeEventListener('click', onClick);
					else if (this.detachEvent) this.detachEvent('onmousemove', onClick);
				});
			}
		}
	});

	// encrypted element
	this.element = document.registerElement(_encryptedElementName, {prototype: this.elementPrototype});

	// encrypted link
	document.addEventListener("DOMContentLoaded", function() {
		var onClick = function(e) {
			// add the decryption form into the DOM shadow
			var templateForDecryption = document.querySelector(_templateForDecryptionSelector);
			var nodeForDecryption = templateForDecryption.content.cloneNode(true);
			// event handler functions cannot reference the local "this" variable
			var _this = this;
			nodeForDecryption.querySelector('form').onsubmit = function() {
				encryptedElement.decryptHrefByPassword(_this, this.elements[0].value).catch(alert);
				// do not submit the form
				return false;
			};
			this.appendChild(nodeForDecryption);
			// disable the onClick event handler as the form should not be added multiple-times
			if (this.removeEventListener) this.removeEventListener('click', onClick);
			else if (this.detachEvent) this.detachEvent('onmousemove', onClick);
			// make the link non-click-able
			this.style.pointerEvents = 'none';
			// do not follow the link's target
			return false;
		}
		var encryptedAElements = document.querySelectorAll(_encryptedAElementSelector);
		for (var i = 0; i < encryptedAElements.length; i++) {
			encryptedAElements[i].addEventListener('click', onClick);
			console.log(encryptedAElements[i]);
		}
	});

	// decrypt encrypted <a> element's href attribute
	this.decryptHrefByPassword = function(aElement, password) {
		return this.decryptHrefByPasswordArrayBuffer(aElement, new TextEncoder('utf-8').encode(password));
	}
	this.decryptHrefByPasswordArrayBuffer = function(aElement, passwordArrayBuffer) {
		// call-back functions cannot reference the local "this" variable
		var _this = this;
		// convert Data URI in the href attribute and base64-encoded encryption parameter values to ArrayBuffer values
		return Promise.all([
			// data
			encryptedElement.dataUriToArrayBuffer(aElement.getAttribute('href')),
			// salt
			aElement.hasAttribute(_saltAttributeName)
			? encryptedElement.base64ToArrayBuffer(aElement.getAttribute(_saltAttributeName))
			: _defaultSaltArrayBuffer,
			// iv
			aElement.hasAttribute(_ivAttributeName)
			? encryptedElement.base64ToArrayBuffer(aElement.getAttribute(_ivAttributeName))
			: _defaultIvArrayBuffer
		]).then(function(arrayBufferValues) {
			// decrypt the encoded binary data by values of encryption parameters
			return encryptedElement.decryptor(arrayBufferValues[0], arrayBufferValues[1], arrayBufferValues[2], passwordArrayBuffer);
		}).then(function(url) {
			// update the href attribute by the decrypted data, remove attributes of encryption parameters, and make it click-able
			aElement.setAttribute('href', url);
			aElement.removeAttribute(_saltAttributeName);
			aElement.removeAttribute(_ivAttributeName);
			aElement.style.pointerEvents = '';
		});
		return promise;
	};

	// decrypt all encrypted elements
	this.decryptAllElementsByPassword = function(password) {
		var encryptedElements = document.getElementsByTagName(_encryptedElementName);
		var promises = new Array(encryptedElements.length);
		for (var i = 0; i < encryptedElements.length; i++) {
			promises[i] = encryptedElements[i].decryptByPassword(password);
		}
		return Promise.all(promises);
	}
	this.decryptAllElementsByPasswordArrayBuffer = function(passwordArrayBuffer) {
		var encryptedElements = document.getElementsByTagName(_encryptedElementName);
		var promises = new Array(encryptedElements.length);
		for (var i = 0; i < encryptedElements.length; i++) {
			promises[i] = encryptedElements[i].decryptByPasswordArrayBuffer(passwordArrayBuffer);
		}
		return Promise.all(promises);
	}

	// decrypt all <a> elements' href attributes
	this.decryptAllHrefsByPassword = function(password) {
		var encryptedAElements = document.querySelectorAll(_encryptedAElementSelector);
		var promises = new Array(encryptedAElements.length);
		for (var i = 0; i < encryptedAElements.length; i++) {
			promises[i] = this.decryptHrefByPassword(encryptedAElements[i], password);
		}
		return Promise.all(promises);
	}
	this.decryptAllHrefsByPasswordArrayBuffer = function(passwordArrayBuffer) {
		var encryptedAElements = document.querySelectorAll(_encryptedAElementSelector);
		var promises = new Array(encryptedAElements.length);
		for (var i = 0; i < encryptedAElements.length; i++) {
			promises[i] = this.decryptHrefByPasswordArrayBuffer(encryptedAElements[i], passwordArrayBuffer);
		}
		return Promise.all(promises);
	}

	// decrypt both all encrypted elements and all <a> elements' href attributes
	this.decryptAllByPasswordOnSubmit = function(form) {
		Promise.all([
			this.decryptAllElementsByPassword(form.elements[0].value),
			this.decryptAllHrefsByPassword(form.elements[0].value)
		]).then(function() {
			if (
				(document.getElementsByTagName(_encryptedElementName).length == 0)
				&& (document.querySelectorAll(_encryptedAElementSelector).length == 0)
			) form.parentNode.removeChild(form);
		}, alert);
		return false;
	}

	// decryption function
	this.decryptor = function(dataArrayBuffer, saltArrayBuffer, ivArrayBuffer, passwordArrayBuffer) {
		// creating a promise to allow callbacks on async functions
		var promise = new Promise(function(resolve, reject) {
			// create a PBKDF2 key containing the password
			window.crypto.subtle.importKey(
				'raw',
				passwordArrayBuffer,
				{'name': 'PBKDF2'},
				false,
				['deriveKey']
			// derive a key from the password
			).then(
				function(baseKey) {
					return window.crypto.subtle.deriveKey(
						{ // algorithm
							'name': 'PBKDF2',
							'salt': saltArrayBuffer,
							'iterations': 1000,
							'hash': 'SHA-1'
						},
						baseKey,
						{ // key we want
							'name': 'AES-CBC',
							'length': 256
						},
						true, // extrable
						['encrypt', 'decrypt'] // for new key
					);
				}, function(error) {
					reject(new Error("Error in window.crypto.subtle.importKey():\n" + error.message));
				}
			// decrypt a cryptotext by the key into a plaintext
			).then(
				function(aesKey) {
					return window.crypto.subtle.decrypt(
						{ // algorithm
							'name': 'AES-CBC',
							'iv': ivArrayBuffer
						},
						aesKey, // key
						dataArrayBuffer // cryptotext
					);
				}, function(error) {
					reject(new Error("Error in window.crypto.subtle.deriveKey():\n" + error.message));
				}
			// call-back via Promise API on the decrypted plain-text UTF-8 string
			).then(
				function(decrypted) {
					resolve(new TextDecoder('utf-8').decode(new DataView(decrypted)));
				}, function(error) {
					reject(new Error("Error in window.crypto.subtle.decrypt():\n"
						+ (error.message.length ? error.message : "wrong password")));
				}
			);
		});
		return promise;
	}

	// Data URI and Base64 decoding async functions
	this.dataUriToArrayBuffer = function(dataUri) {
		return new Promise(function(resolve, reject) {
			var req = new XMLHttpRequest();
			req.open('GET', dataUri);
			req.responseType = 'arraybuffer';
			req.onload = function() {
				if (req.status == 200) {
					resolve(req.response);
				} else {
					reject(new Error("Error in XMLHttpRequest.onload for Data URI decoding: " + req.statusText));
				}
			};
			req.onerror = function() {
				reject(new Error("Error in XMLHttpRequest.onerror for Data URI decoding"));
			};
			req.send();
		});
	}
	this.base64ToArrayBuffer = function(base64Encoded) {
		return this.dataUriToArrayBuffer("data:application/octet-stream;base64," + base64Encoded);
	}

}
